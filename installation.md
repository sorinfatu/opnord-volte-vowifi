# Installation of needed components

If you obtained the platform-tools zip file, this needs to be extracted. Make sure you can get to it decently easy.

You will have to be able to get either a command prompt in that folder or a PowerShell, since we will need to run adb.exe from there and that is a command line utility.

To open a PowerShell in the folder you can `Shift + Right Click` in the folder and choose PowerShell from the context menu. Otherwise just open PowerShell from the start menu and navigate to the contents of the platform tools folder.

If you are using the Command Prompt, the actual commands are `adb devices`
If you are using PowerShell, the commands are `./adb devices` (note the `./`)

Make sure you can run adb. Doing `adb devices` should result in something like this
```
	* daemon not running; starting now at tcp:5037
	* daemon started successfully
```	

Second priority is to install QPST - This is a tool for interracting with the device on a more bare-bones level. It can be installed from here [QPST.2.7.496.1.zip](tools/QPST.2.7.496.1.zip)

Take care on what changes you make with all the applications in this pack as it can lead to bricking your phone - We will only be using PDC to change the profile for the network connection.

make sure you have [drivers_unrecognised](tools/drivers_unrecognised.zip) and  [Qualcomm_USB_Driver_v1.0](tools/Qualcomm_USB_Driver_v1.0.zip) downloaded and unzipped.

**Steps:**
---
( The commands listed below are in the simple format - if using Powershell see above)

1. `adb devices` - make sure you see the device listed
2. `adb reboot ftm` - the device will reboot, some chineese characters on a black background will be visible and shortly after the screen will turn black
3. `adb shell` - once the screen goes black use this untill you get a `Nord` prompt
4. `setprop sys.usb.config diag,diag_mdm,qdss,qdss_mdm,serial_cdev,dpl,rmnet,adb` - once in the the Nord adb shell use this command to enable a debug mode - note that the screen will remain black but the device is powered on.
5. drivers - if the command from 4. worked you should have a few devices show in windows device manager - in my case these were not recognised and automatic driver update did not work, however if you right click on them and use update driver, then select from computer and point it to the unzipped folder of `drivers_unrecognised` you should be able to install all of them. You might also want to install the Qualcomm USB driver at this point from Qualcomm_USB_Driver_v1.0.zip
6. PDC - once the steps above are done, open up PDC from the start menu and you should be able to select the modem from the dropdown box. Once this is done you will see a list with all profiles the device selects. Right click on the one you wish to use and select it for `Sub 0` and `Sub 1` (this will refresh the list each time, don't worry) then press Activate
7. `adb reboot` - if you get the new profile active for both Subs you can quit PDC, and in the Powershell or command prompt use `adb reboot`

For the profile to use see [profile.md](profile.md)

Once the device reboots you should have VoLTE and VoWIFI. If the toggles for them are not present in the menu, download and install [com.oem.oemlogkit](tools/com.oem.oemlogkit_1.0-1.zip) in the device (it's an apk in the zip file).

After installation, go to the dialer and dial `*#800#` - this should open the secret menu. Go to `Function Switch` and toggle on both VoLTE and VoWIFI.

In case of system updates braking the functionality - the steps need to be redone (without the driver installation obviously)

