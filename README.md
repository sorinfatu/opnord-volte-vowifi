# Enable VoLTE and VoWIFI on OnePlus Nord ( AC2003 )

This is a basic guide on how to enable VoLTE and VoWIFI on the OnePlus Nord.

It has been tested on OnePlus Nord AC2003 with OxygenOS 11.1.1.1 11.1.1.2 11.1.1.3 11.1.4.4 11.1.5.5 11.1.6.6 - so Android 11

The procedure outlined here does **NOT** require and unlocked bootloader or a rooted device.

It might work for other models but it is nothing that I can test.

**I am not the author of any binary here, I just put toghether the guide and instructions**

---
**Requirements:**
- OnePlus Nord AC2003 - updated to Android 11 ( >= 11.1.1.1 )
- computer with USB port and running MS Windows
- USB cable - preferabbly the original one
- drivers - most (if not all) of them are in the `tools` folder in this repo
---
**Getting started**

Obtain the latest Android Platform Tools from Google `https://dl.google.com/android/repository/platform-tools-latest-windows.zip`

This should always point to the newest one.

Obtain the zip files from the tools folder in this repo. As of 30.03.2021 these work with both 11.1.1.1 and 11.1.1.2 OxygenOS


**I have not tested this on multiple computers, but from experience with my old device (Xiaomi Mi8) you might encounter issues if doing this on a system with an AMD Ryzen CPU. To get it working on such systems, you need to use an USB Hub instead of connecting directly to one of the built in USB ports of the computer. No issues on Intel CPUs.**

Please note that this procedure will only have the desired effect if your SIM is capable of VoLTE/VoWIFI and if it is enabled by your carrier. Some carriers only provission VoLTE on certain phone models so, as long as you can make sure you have those services activated on your subscription/SIM you're good to go.

I have tested this on my own device, with a single SIM from DigiMobil operator in Romania. 
- both VoLTE and VoWIFI work
- sending SMS while on VoLTE works very fast (faster than on 3G)
- the changes persist through reboots


**Updating the system is possible with these modifications but the updates will brake VoLTE/VoWIFI (at least the latest update did) so the procedure will have to be done again after updating.**

# Please note that I am not responsible for any data loss or damage to devices. Use this at your own risk.

**For the procedure see [installation.md](installation.md)**
